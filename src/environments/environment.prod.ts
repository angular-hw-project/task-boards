export const environment = {
  production: true,
  apiUrl: 'https://task-boards-api.onrender.com/api/'
};
