export default interface RegisterBody {
  email: string,
  username: string,
  password: string,
}
