export default interface Filter {
  filter_by: string,
  filter: string
}
