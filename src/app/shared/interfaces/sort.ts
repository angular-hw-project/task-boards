export default interface Sort {
  sort_by: string,
  asc: boolean,
}
