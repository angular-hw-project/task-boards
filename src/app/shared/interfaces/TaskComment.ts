export default interface TaskComment {
  _id: string,
  task_id: string,
  created_by: string,
  text: string,
  createdDate: string,
}
