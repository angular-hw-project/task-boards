import TaskComment from './TaskComment';

export default interface Task {
  _id: string,
  created_by: string,
  name: string,
  status: 'new' | 'inProgress' | 'done' | '',
  archived: boolean,
  createdDate: string,
  comments: TaskComment[]
}
