export default interface ModalData {
  mode: 'new' | 'edit' | 'delete',
  id: string,
  name: string,
  description: string
}
