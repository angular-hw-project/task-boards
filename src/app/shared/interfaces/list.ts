import Task from './task';

export default interface List {
  name: string,
  status: Task['status'],
  color: string,
  archived: boolean,
  tasks: Task[]
}
