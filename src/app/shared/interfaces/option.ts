export default interface Option {
  alias: string,
  description: string
}
