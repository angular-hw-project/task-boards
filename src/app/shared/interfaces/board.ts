import Task from './task';

export default interface Board {
  _id: string,
  created_by: string,
  name: string,
  description: string,
  color_scheme: {
    new: string,
    inProgress: string,
    done: string
  },
  tasks: {
    new: Task[],
    inProgress: Task[],
    done: Task[]
    archived: Task[]
  }
  createdDate: string,
}
