export default interface FetchMessage {
  status: 'error' | 'success',
  message: string
}
