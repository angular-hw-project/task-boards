import User from './user';
import Task from './task';

export default interface Comment {
  _id: string,
  created_by: User,
  task_id: Task,
  text: string,
  createdDate: string
}
