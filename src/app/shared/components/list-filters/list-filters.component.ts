import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import Option from '@shared/interfaces/option';
import Filter from '@shared/interfaces/filter';
import { RestApiService } from '@core/services/rest-api.service';
import Sort from '@shared/interfaces/sort';
import { Observable } from 'rxjs';

@Component({
  selector: 'app-list-filters',
  templateUrl: './list-filters.component.html',
  styleUrls: ['./list-filters.component.scss']
})
export class ListFiltersComponent implements OnInit {
  @Input() filterOptions: Option[] = [];
  @Input() sortOptions: Option[] = [];
  @Input() defaultFilter!: Filter;
  @Input() defaultSort!: Sort;

  @Output() changeFilterEvent = new EventEmitter<Filter>()
  @Output() changeSortEvent = new EventEmitter<Sort>()

  filterForm!: FormGroup;
  sortForm!: FormGroup;

  order!: boolean;

  constructor(
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.order = this.defaultSort.asc;
    this.filterForm = this.fb.group({
      filter_by: new FormControl(this.defaultFilter.filter_by, [
        Validators.required
      ]),
      filter: new FormControl(this.defaultFilter.filter, [
        Validators.required,
        Validators.minLength(2)
      ])
    });

    this.sortForm = this.fb.group({
      sort_by: new FormControl(this.defaultSort.sort_by, [
        Validators.required
      ])
    });

    const filterForm$: Observable<Filter> = this.filterForm.valueChanges.pipe(
      debounceTime(1000),
      distinctUntilChanged((prev, curr) => {
        return (
          prev.filter_by === curr.filter_by &&
          prev.filter === curr.filter
        );
      }),
      map((formData) => {
        return { filter_by: formData.filter_by, filter: formData.filter };
      })
    );

    const sortForm$ = this.sortForm.valueChanges.pipe(
      distinctUntilChanged((prev, curr) => prev.sort_by === curr.sort_by),
      map((formData) => {
        return { sort_by: formData.sort_by, asc: this.order };
      })
    );

    filterForm$.subscribe((formData: Filter) => {
      this.changeFilterEvent.emit(formData);
    });

    sortForm$.subscribe((formData: Sort) => {
      this.changeSortEvent.emit(formData);
    });
  }

  toggleOrder() {
    this.order = !this.order;
    this.changeSortEvent.emit({
      sort_by: this.sortForm.value.sort_by,
      asc: this.order
    });
  }

  resetFilters() {
    this.filterForm.reset({
      filter_by: this.defaultFilter.filter_by,
      filter: this.defaultFilter.filter
    });
    this.sortForm.reset({ sort_by: this.defaultSort.sort_by });
    this.order = this.defaultSort.asc;
  }

}
