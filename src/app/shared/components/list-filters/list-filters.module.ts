import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListFiltersComponent } from './list-filters.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ButtonModule } from '@shared/components/button/button.module';



@NgModule({
  declarations: [
    ListFiltersComponent
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        ButtonModule
    ],
  exports: [
    ListFiltersComponent
  ]
})
export class ListFiltersModule { }
