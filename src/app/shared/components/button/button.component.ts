import { ChangeDetectionStrategy, Component, EventEmitter, Input, Output } from '@angular/core';

@Component({
  selector: 'app-button',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './button.component.html',
  styleUrls: ['./button.component.scss']
})
export class ButtonComponent {
  @Input() text: string = 'button';
  @Input() type: 'main' | 'secondary' | 'warning' | 'danger' | 'success' = 'main';
  @Input() size: 'small' | 'medium' | 'large' = 'medium';
  @Input() btnDisabled: boolean = false;

  @Output() btnClick = new EventEmitter<void>();

  constructor() { }

  emitClick(): void {
    this.btnClick.emit();
  }

}
