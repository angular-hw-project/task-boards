import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import ModalData from '@shared/interfaces/modalData';
import { RestApiService } from '@core/services/rest-api.service';
import { DashboardService } from './dashboard.service';

@Injectable({
  providedIn: 'root'
})
export class BoardModalService {
  private _queryResult$: BehaviorSubject<string>;
  private _modalHidden$: BehaviorSubject<boolean>;
  private _modalData$: BehaviorSubject<ModalData>;

  fetching$: Observable<boolean> = this.restApi.fetching$;
  queryResult$: Observable<string>;
  modalHidden$: Observable<boolean>;
  modalData$: Observable<ModalData>;

  constructor(
    private restApi: RestApiService,
    private dashboardService: DashboardService
  ) {
    // Initial values for observables
    const initialQueryResult: string  = '';
    const initialModalHidden: boolean = true;
    const initialModalData: ModalData = { mode: 'new', id: '', name: '', description: '' };

    this._queryResult$ = new BehaviorSubject<string>(initialQueryResult);
    this._modalHidden$ = new BehaviorSubject<boolean>(initialModalHidden);
    this._modalData$ = new BehaviorSubject<ModalData>(initialModalData);

    this.queryResult$ = this._queryResult$.asObservable();
    this.modalHidden$ = this._modalHidden$.asObservable();
    this.modalData$ = this._modalData$.asObservable();

  }

  get fetching(): boolean {
    return this.restApi.fetching;
  }

  setFetching(nextState: boolean): void {
    this.restApi.setFetching(nextState);
  }

  get queryResult(): string {
    return this._queryResult$.getValue();
  }

  setQueryResult(nextState: string): void {
    this._queryResult$.next(nextState);
  }

  get modalHidden(): boolean {
    return this._modalHidden$.getValue();
  }

  setModalHidden(nextState: boolean): void {
    this._modalHidden$.next(nextState);
    if (nextState) this.setQueryResult('');
  }

  get modalData(): ModalData {
    return this._modalData$.getValue();
  }

  setModalData(nextState: ModalData): void {
    if (JSON.stringify(this.modalData) === JSON.stringify(nextState)) {
      return;
    }

    this._modalData$.next(nextState);
  }

  newBoard(data: { name: string, description: string }): void {
    this.setFetching(true);
    this.restApi.newBoard(data)
      .subscribe(res => {
        this.setQueryResult(res.message);
        this.setFetching(false);
        this.dashboardService.fetchBoards();
      });
  }

  editBoard(id: string, data: { name: string, description: string }): void {
    this.setFetching(true);
    this.restApi.editBoard(id, data)
      .subscribe(res => {
        this.setQueryResult(res.message);
        this.setFetching(false);
        this.dashboardService.fetchBoards();
      });
  }

  deleteBoard(id: string): void {
    this.setFetching(true);
    this.restApi.deleteBoard(id)
      .subscribe(res => {
        this.setQueryResult(res.message);
        this.setFetching(false);
        this.dashboardService.fetchBoards();
      });
  }

}
