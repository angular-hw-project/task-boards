import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import Filter from '@shared/interfaces/filter';
import Sort from '@shared/interfaces/sort';
import Board from '@shared/interfaces/board';
import { RestApiService } from '@core/services/rest-api.service';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {
  private _filter$: BehaviorSubject<Filter>;
  private _sort$: BehaviorSubject<Sort>;
  private _boards$: BehaviorSubject<Board[]>;

  fetching$: Observable<boolean> = this.restApi.fetching$;
  filter$: Observable<Filter>;
  sort$: Observable<Sort>;
  boards$: Observable<Board[]>;
  defaultFilter: Filter = { filter_by: 'name', filter: '' };
  defaultSort: Sort = { sort_by: 'createdDate', asc: false }

  constructor(
    private restApi: RestApiService
  ) {
    // Initial values for observables
    const initialFilter: Filter     = this.defaultFilter;
    const initialSort: Sort         = this.defaultSort;
    const initialBoards: Board[]    = [];

    this._filter$ = new BehaviorSubject<Filter>(initialFilter);
    this._sort$ = new BehaviorSubject<Sort>(initialSort);
    this._boards$ = new BehaviorSubject<Board[]>(initialBoards);

    this.filter$ = this._filter$.asObservable();
    this.sort$ = this._sort$.asObservable();
    this.boards$ = this._boards$.asObservable();
    this.fetchBoards();
  }

  get fetching(): boolean {
    return this.restApi.fetching;
  }

  setFetching(nextState: boolean): void {
    this.restApi.setFetching(nextState);
  }

  get filter(): Filter {
    return this._filter$.getValue();
  }

  setFilter(nextState: Filter): void {
    if (JSON.stringify(this.filter) === JSON.stringify(nextState)) {
      return;
    }
    this._filter$.next(nextState);
    this.fetchBoards();
  }

  get sort(): Sort {
    return this._sort$.getValue();
  }

  setSort(nextState: Sort): void {
    this._sort$.next(nextState);
  }

  get boards(): Board[] {
    return this._boards$.getValue();
  }

  fetchBoards(): void {
    this.setFetching(true);
    this.restApi.getBoards(this.filter)
      .subscribe(boards => {
        this._boards$.next(boards);
        this.setFetching(false);
      });
  }

}
