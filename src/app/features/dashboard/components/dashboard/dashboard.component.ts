import { Component } from '@angular/core';
import Option from '@shared/interfaces/option';
import Filter from '@shared/interfaces/filter';
import { DashboardService } from '../../services/dashboard.service';
import Sort from '@shared/interfaces/sort';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent {
  filterOptions: Option[] = [
    { alias: 'name', description: 'Board name' },
    { alias: 'taskName', description: 'Task name' }
  ];
  sortOptions: Option[] = [
    { alias: 'createdDate', description: 'Creation date' },
    { alias: 'name', description: 'Task name' },
    { alias: 'new', description: 'Task "new" count' },
    { alias: 'inProgress', description: 'Task "in progress" count' },
    { alias: 'done', description: 'Task "done" count' }
  ];

  constructor(
    private dashboardService: DashboardService,
    private authService: AuthService
  ) { }

  get defaultFilter(): Filter {
    return this.dashboardService.defaultFilter;
  }

  get defaultSort(): Sort {
    return this.dashboardService.defaultSort;
  }

  setFilter(filter: Filter): void {
    this.dashboardService.setFilter(filter);
  }

  setSort(sort: Sort): void {
    this.dashboardService.setSort(sort);
  }

  isLoggedIn(): boolean {
    return this.authService.isLoggedIn();
  }

}
