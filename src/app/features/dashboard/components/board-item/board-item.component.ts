import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import Board from '@shared/interfaces/board';
import { Router } from '@angular/router';
import { BoardModalService } from '../../services/board-modal.service';

@Component({
  selector: 'app-board-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './board-item.component.html',
  styleUrls: ['./board-item.component.scss']
})
export class BoardItemComponent {
  @Input() board!: Board;

  showMenu: boolean = false;

  constructor(
    private router: Router,
    private modalService: BoardModalService,
  ) { }

  toggleMenu() {
    this.showMenu = !this.showMenu;
  }

  newBoard() {
    this.modalService.setModalData({
      mode: 'new',
      id: '',
      name: '',
      description: ''
    });
    this.modalService.setModalHidden(false);
    this.showMenu = false;
  }

  editBoard() {
    this.modalService.setModalData({
      mode: 'edit',
      id: this.board._id,
      name: this.board.name,
      description: this.board.description
    });
    this.modalService.setModalHidden(false);
    this.showMenu = false;
  }

  deleteBoard() {
    this.modalService.setModalData({
      mode: 'delete',
      id: this.board._id,
      name: this.board.name,
      description: this.board.description
    });
    this.modalService.setModalHidden(false);
    this.showMenu = false;
  }

  openBoard() {
    if (this.board) this.router.navigate([`/board-details/${this.board._id}`]);
  }

}
