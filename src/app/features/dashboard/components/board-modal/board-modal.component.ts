import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { BoardModalService } from '../../services/board-modal.service';
import { Observable } from 'rxjs';
import ModalData from '@shared/interfaces/modalData';

@Component({
  selector: 'app-board-modal',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './board-modal.component.html',
  styleUrls: ['./board-modal.component.scss']
})
export class BoardModalComponent implements OnInit {
  fetching$: Observable<boolean> = this.modalService.fetching$;
  queryResult$: Observable<string> = this.modalService.queryResult$;
  hidden$: Observable<boolean> = this.modalService.modalHidden$;
  data: ModalData = this.modalService.modalData;

  //ngModel
  name: string = '';
  description: string = '';

  constructor(
    private modalService: BoardModalService
  ) { }

  ngOnInit(): void {
    // Receive board data and populate form inputs.
    this.modalService.modalData$.subscribe(newData => {
      this.data = newData;
      this.name = this.data.name;
      this.description = this.data.description;
    });
  }

  hideModal() {
    this.modalService.setModalHidden(true);
  }

  newBoard() {
    this.modalService.newBoard({
      name: this.name,
      description: this.description
    });
  }

  editBoard() {
    this.modalService.editBoard(this.data.id, {
      name: this.name,
      description: this.description
    });
  }

  deleteBoard() {
    this.modalService.deleteBoard(this.data.id);
  }
}
