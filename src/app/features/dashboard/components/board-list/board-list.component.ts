import { Component, Input, OnInit } from '@angular/core';
import Board from '@shared/interfaces/board';
import { RestApiService } from '@core/services/rest-api.service';
import { DashboardService } from '../../services/dashboard.service';
import { Observable } from 'rxjs';
import { BoardModalService } from '../../services/board-modal.service';
import Sort from '@shared/interfaces/sort';

@Component({
  selector: 'app-board-list',
  templateUrl: './board-list.component.html',
  styleUrls: ['./board-list.component.scss']
})
export class BoardListComponent implements OnInit {
  fetching$: Observable<boolean> = this.dashboardService.fetching$;
  boards: Board[] = this.dashboardService.boards;
  sort: Sort = this.dashboardService.sort;

  constructor(
    private dashboardService: DashboardService,
  ) { }

  ngOnInit() {
    this.dashboardService.sort$.subscribe((sort) => {
      this.sort = sort;
    });
    this.dashboardService.boards$.subscribe((boards) => {
      this.boards = boards;
    });
    this.dashboardService.fetchBoards();
  }

}
