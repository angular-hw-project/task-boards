import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { DashboardComponent } from './components/dashboard/dashboard.component';
import { BoardListComponent } from './components/board-list/board-list.component';
import { BoardItemComponent } from './components/board-item/board-item.component';
import { SortBoardsPipe } from './pipes/sort-boards.pipe';
import { BoardModalComponent } from './components/board-modal/board-modal.component';
import { FormsModule } from '@angular/forms';
import { ListFiltersModule } from '@shared/components/list-filters/list-filters.module';
import { ButtonModule } from '@shared/components/button/button.module';



@NgModule({
  declarations: [
    DashboardComponent,
    BoardListComponent,
    BoardItemComponent,
    SortBoardsPipe,
    BoardModalComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ListFiltersModule,
    ButtonModule
  ],
  exports: [
    DashboardComponent
  ]
})
export class DashboardModule { }
