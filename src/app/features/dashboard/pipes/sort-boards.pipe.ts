import { Pipe, PipeTransform } from '@angular/core';
import Board from '@shared/interfaces/board';

@Pipe({
  name: 'sortBoards'
})
export class SortBoardsPipe implements PipeTransform {

  transform(value: Board[], property: string, order: boolean): Board[] {
    const sortByDate = (a: Board, b: Board): number => {
      return new Date(a.createdDate).valueOf() - new Date(b.createdDate).valueOf();
    }
    const sortByName = (a: Board, b: Board): number => {
      const [nameA, nameB] = [a.name.toUpperCase(), b.name.toUpperCase()];
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    }
    const sortByNew = (a: Board, b: Board): number => {
      return a.tasks.new.length - b.tasks.new.length;
    }
    const sortByInProgress = (a: Board, b: Board): number => {
      return a.tasks.inProgress.length - b.tasks.inProgress.length;
    }
    const sortByDone = (a: Board, b: Board): number => {
      return a.tasks.done.length - b.tasks.done.length;
    }

    if (property === 'createdDate') value.sort(sortByDate);
    if (property === 'name') value.sort(sortByName);
    if (property === 'new') value.sort(sortByNew);
    if (property === 'inProgress') value.sort(sortByInProgress);
    if (property === 'done') value.sort(sortByDone);

    if (!order) value = value.reverse();
    return value;
  }

}
