import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { AuthService } from '@core/services/auth.service';
import FetchMessage from '@shared/interfaces/fetchMessage';
import { Observable, Subscription } from 'rxjs';

@Component({
  selector: 'app-register',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit {
  registerForm!: FormGroup;
  fetchMessage$: Observable<FetchMessage> = this.authService.fetchMessage$;
  fetching$: Observable<boolean> = this.authService.fetching$;

  constructor(
    private authService: AuthService,
    private fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.authService.setFetchMessage({ status: 'error', message: '' });

    this.registerForm = this.fb.group({
      email: new FormControl('', [
        Validators.required,
        Validators.email
      ]),
      username: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(50)
      ]),
      password: new FormControl('', [
        Validators.required,
        Validators.minLength(6),
        Validators.maxLength(100)
      ]),
      repeatPassword: new FormControl('', [
        Validators.required
      ])
    });
  }

  get email(): AbstractControl<any, any> | null {
    return this.registerForm.get('email');
  }
  get username(): AbstractControl<any, any> | null {
    return this.registerForm.get('username');
  }
  get password(): AbstractControl<any, any> | null {
    return this.registerForm.get('password');
  }
  get repeatPassword(): AbstractControl<any, any> | null {
    return this.registerForm.get('repeatPassword');
  }

  attemptRegister() {
    const credentials = { email: this.email?.value, username: this.username?.value, password: this.password?.value };
    this.authService.register(credentials);
  }

}
