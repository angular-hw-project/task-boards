import { Pipe, PipeTransform } from '@angular/core';
import Task from '@shared/interfaces/task';

@Pipe({
  name: 'sortTasks'
})
export class SortTasksPipe implements PipeTransform {

  transform(value: Task[], property: string | null, order: boolean | null): Task[] {
    const sortByDate = (a: Task, b: Task): number => {
      return new Date(a.createdDate).valueOf() - new Date(b.createdDate).valueOf();
    }
    const sortByName = (a: Task, b: Task): number => {
      const [nameA, nameB] = [a.name.toUpperCase(), b.name.toUpperCase()];
      if (nameA < nameB) {
        return -1;
      }
      if (nameA > nameB) {
        return 1;
      }
      return 0;
    }
    if (property === null) property = 'createdDate';
    if (order === null) order = false;

    if (property === 'createdDate') value.sort(sortByDate);
    if (property === 'name') value.sort(sortByName);

    if (!order) value = value.reverse();
    return value;
  }

}
