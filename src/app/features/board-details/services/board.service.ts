import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import Filter from '@shared/interfaces/filter';
import Sort from '@shared/interfaces/sort';
import Board from '@shared/interfaces/board';
import { RestApiService } from '@core/services/rest-api.service';
import Task from '@shared/interfaces/task';
import List from '@shared/interfaces/list';

@Injectable({
  providedIn: 'root'
})
export class BoardService {
  private _filter$: BehaviorSubject<Filter>;
  private _sort$: BehaviorSubject<Sort>;
  private _board$: BehaviorSubject<Board | null>;
  private _boardId$: BehaviorSubject<string>;
  private _boardNotFound$: BehaviorSubject<boolean>;
  private _tasks$: BehaviorSubject<Task[]>;
  private _lists$: BehaviorSubject<List[]>;
  private _archivedList$: BehaviorSubject<List>;

  fetching$: Observable<boolean> = this.restApi.fetching$;
  filter$: Observable<Filter>;
  sort$: Observable<Sort>;
  board$: Observable<Board | null>;
  boardId$: Observable<string>;
  boardNotFound$: Observable<boolean>;
  tasks$: Observable<Task[]>;
  lists$: Observable<List[]>;
  archivedList$: Observable<List>;

  defaultFilter: Filter = { filter_by: 'name', filter: '' };
  defaultSort: Sort = { sort_by: 'createdDate', asc: false };

  constructor(
    private restApi: RestApiService
  ) {
    // Initial values for observables
    const initialFilter: Filter      = this.defaultFilter;
    const initialSort: Sort          = this.defaultSort;
    const initialBoard: Board | null = null;
    const initialTasks: Task[]       = [];
    const initialLists: List[]       = [
      { name: 'New', status: 'new', archived: false, color: '', tasks: [] },
      { name: 'In Progress', status: 'inProgress', archived: false, color: '', tasks: [] },
      { name: 'Done', status: 'done', archived: false, color: '', tasks: [] }
    ];
    const initialArchivedList: List  = { name: 'Archived', status: '', archived: true, color: '#737373', tasks: [] };

    this._filter$ = new BehaviorSubject<Filter>(initialFilter);
    this._sort$ = new BehaviorSubject<Sort>(initialSort);
    this._board$ = new BehaviorSubject<Board | null>(initialBoard);
    this._boardId$ = new BehaviorSubject<string>('');
    this._boardNotFound$ = new BehaviorSubject<boolean>(false);
    this._tasks$ = new BehaviorSubject<Task[]>(initialTasks);
    this._lists$ = new BehaviorSubject<List[]>(initialLists);
    this._archivedList$ = new BehaviorSubject<List>(initialArchivedList);

    this.filter$ = this._filter$.asObservable();
    this.sort$ = this._sort$.asObservable();
    this.boardId$ = this._boardId$.asObservable();
    this.board$ = this._board$.asObservable();
    this.boardNotFound$ = this._boardNotFound$.asObservable();
    this.tasks$ = this._tasks$.asObservable();
    this.lists$ = this._lists$.asObservable();
    this.archivedList$ = this._archivedList$.asObservable();
  }

  get fetching(): boolean {
    return this.restApi.fetching;
  }

  setFetching(nextState: boolean): void {
    this.restApi.setFetching(nextState);
  }

  get filter(): Filter {
    return this._filter$.getValue();
  }

  setFilter(nextState: Filter): void {
    if (JSON.stringify(this.filter) === JSON.stringify(nextState)) {
      return;
    }
    this._filter$.next(nextState);
    this.fetchBoard();
  }

  get sort(): Sort {
    return this._sort$.getValue();
  }

  setSort(nextState: Sort): void {
    this._sort$.next(nextState);
  }

  get board(): Board | null {
    return this._board$.getValue();
  }

  fetchBoard(): void {
    if (this.boardId === '') return;
    this.restApi.getBoard(this.boardId)
      .subscribe(board => {
        if (board === null) {
          this.setBoardNotFound(true);
        } else {
          this._board$.next(board);
          this.fetchTasks();
        }
      });
  }

  get boardId(): string {
    return this._boardId$.getValue();
  }

  setBoardId(nextState: string): void {
    this._boardId$.next(nextState);
  }

  get boardNotFound(): boolean {
    return this._boardNotFound$.getValue();
  }

  private setBoardNotFound(nextState: boolean): void {
    this._boardNotFound$.next(nextState);
  }

  get tasks(): Task[] {
    return this._tasks$.getValue();
  }

  fetchTasks(): void {
    if (this.boardNotFound) return;
    this.restApi.getTasks(this.boardId, this.filter)
      .subscribe(tasks => {
        this._tasks$.next(tasks);
        this.populateTaskLists(this.tasks);
      });
  }

  get lists(): List[] {
    return this._lists$.getValue();
  }

  get archivedList(): List {
    return this._archivedList$.getValue();
  }

  private populateTaskLists(tasks: Task[]): void {
    if (this.board === null) return;
    let _lists = this.lists;
    let _archivedList = this.archivedList;
    _lists[0].color = this.board.color_scheme.new;
    _lists[1].color = this.board.color_scheme.inProgress;
    _lists[2].color = this.board.color_scheme.done;
    _lists[0].tasks = this.extractTasks(tasks, 'new');
    _lists[1].tasks = this.extractTasks(tasks, 'inProgress');
    _lists[2].tasks = this.extractTasks(tasks, 'done');
    _archivedList.tasks = this.extractTasks(tasks, '', true);
    this._lists$.next(_lists);
    this._archivedList$.next(_archivedList);
  }

  private extractTasks(tasks: Task[], status: string, archived: boolean = false): Task[] {
    let _filteredTasks: Task[];
    if (status === '') {
      _filteredTasks = tasks.filter(task => task.archived === archived);
    } else {
      _filteredTasks = tasks.filter(task => task.status === status && task.archived === archived);
    }
    return _filteredTasks
  }

  patchBoardColor(status: Task['status'], color: string): void {
    this.restApi.patchBoardColor(this.boardId, status, color).subscribe(() => {
      this.fetchBoard();
    });
  }

  newTask(name: string, status: Task['status']): void {
    this.restApi.newTask(this.boardId, name, status).subscribe(() => {
      this.fetchTasks();
    })
  }

  editTask(id: string, name: string): void {
    this.restApi.editTask(id, name).subscribe(() => {
      this.fetchTasks();
    });
  }

  changeTaskStatus(id: string, status: Task['status']): void {
    this.restApi.changeTaskStatus(id, status).subscribe(() => {
      this.fetchTasks();
    });
  }

  toggleArchivedStatus(id: string): void {
    this.restApi.toggleArchivedStatus(id).subscribe(() => {
      this.fetchTasks();
    });
  }

  deleteTask(id: string): void {
    this.restApi.deleteTask(id).subscribe(() => {
      this.fetchTasks();
    });
  }

}
