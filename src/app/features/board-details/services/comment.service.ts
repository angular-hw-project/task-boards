import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import Task from '@shared/interfaces/task';
import Comment from '@shared/interfaces/comment';
import { RestApiService } from '@core/services/rest-api.service';
import { BoardService } from './board.service';

@Injectable({
  providedIn: 'root'
})
export class CommentService {
  private _selectedTask$: BehaviorSubject<Task | null>;
  private _comments$: BehaviorSubject<Comment[]>;

  fetching$: Observable<boolean> = this.restApi.fetching$;
  selectedTask$: Observable<Task | null>;
  comments$: Observable<Comment[]>;

  constructor(
    private restApi: RestApiService,
    private boardService: BoardService
  ) {
    this._selectedTask$ = new BehaviorSubject<Task | null>(null);
    this._comments$ = new BehaviorSubject<Comment[]>([]);

    this.selectedTask$ = this._selectedTask$.asObservable();
    this.comments$ = this._comments$.asObservable();
  }

  get fetching(): boolean {
    return this.restApi.fetching;
  }

  setFetching(nextState: boolean): void {
    this.restApi.setFetching(nextState);
  }

  get selectedTask(): Task | null {
    return this._selectedTask$.getValue();
  }

  setSelectedTask(nextState: Task): void {
    if (JSON.stringify(this.selectedTask) === JSON.stringify(nextState)) return;
    this._selectedTask$.next(nextState);
    if (this.selectedTask !== null) this.fetchComments();
  }

  get comments(): Comment[] {
    return this._comments$.getValue();
  }

  fetchComments(): void {
    if (this.selectedTask === null) return;
    this.restApi.getComments(this.selectedTask._id).subscribe((comments) => {
      this._comments$.next(comments.sort((a, b) => {
        return new Date(b.createdDate).valueOf() - new Date(a.createdDate).valueOf()
      }));
    })
  }

  newComment(text: string): void {
    if (this.selectedTask === null) return;
    this.restApi.newComment(this.selectedTask._id, text).subscribe(() => {
      this.fetchComments();
      this.boardService.fetchTasks();
    });
  }

  deleteComment(id: string): void {
    if (this.selectedTask === null) return;
    this.restApi.deleteComment(id).subscribe(() => {
      this.fetchComments();
      this.boardService.fetchTasks();
    });
  }
}
