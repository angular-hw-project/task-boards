import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { BoardDetailsRoutingModule } from './board-details-routing.module';
import { TaskBoardComponent } from './components/task-board/task-board.component';
import { SortTasksPipe } from './pipes/sort-tasks.pipe';
import { TaskListComponent } from './components/task-list/task-list.component';
import { TaskItemComponent } from './components/task-item/task-item.component';
import { SetBgColorDirective } from './directives/set-bg-color.directive';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DragDropModule } from '@angular/cdk/drag-drop';
import { CommentStackComponent } from './components/comment-stack/comment-stack.component';
import { CommentItemComponent } from './components/comment-item/comment-item.component';
import { ListFiltersModule } from '@shared/components/list-filters/list-filters.module';
import { ButtonModule } from '@shared/components/button/button.module';


@NgModule({
  declarations: [
    TaskBoardComponent,
    SortTasksPipe,
    TaskListComponent,
    TaskItemComponent,
    SetBgColorDirective,
    CommentStackComponent,
    CommentItemComponent
  ],
  imports: [
    CommonModule,
    BoardDetailsRoutingModule,
    DragDropModule,
    FormsModule,
    ReactiveFormsModule,
    ListFiltersModule,
    ButtonModule
  ],
  exports: [
    SetBgColorDirective
  ],
  providers: [SortTasksPipe]
})
export class BoardDetailsModule { }
