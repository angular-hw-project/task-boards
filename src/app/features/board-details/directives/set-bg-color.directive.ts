import { Directive, ElementRef, Input, OnInit } from '@angular/core';

@Directive({
  selector: '[appSetBgColor]'
})
export class SetBgColorDirective implements OnInit {
  @Input() appSetBgColor!: string;

  constructor(
    private el: ElementRef
  ) {}

  ngOnInit() {
    this.el.nativeElement.style.backgroundColor = this.appSetBgColor;
  }

}
