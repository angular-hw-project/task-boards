import { SetBgColorDirective } from './set-bg-color.directive';
import { Component, DebugElement } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';


@Component({
  selector: 'app-test-component',
  template: '<p [appSetBgColor]="color" >Green background</p>'
})
export class TestComponent {
  color:string = 'green';
}

describe('SetBgColorDirective', () => {
  let component: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  let inputEl: DebugElement;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [
        TestComponent,
        SetBgColorDirective
      ]
    })

    fixture = TestBed.createComponent(TestComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create an instance', () => {
    const directive = new SetBgColorDirective(inputEl);
    expect(directive).toBeTruthy();
  });
});
