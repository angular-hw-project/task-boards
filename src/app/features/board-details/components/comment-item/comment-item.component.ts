import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import Comment from '@shared/interfaces/comment';
import { CommentService } from '../../services/comment.service';
import { Observable } from 'rxjs';
import { AuthService } from '@core/services/auth.service';

@Component({
  selector: 'app-comment-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './comment-item.component.html',
  styleUrls: ['./comment-item.component.scss']
})
export class CommentItemComponent {
  @Input() comment!: Comment;

  fetching$: Observable<boolean> = this.commentService.fetching$;
  user_id: string | undefined = this.authService.user?._id

  constructor(
    private commentService: CommentService,
    private authService: AuthService
  ) { }

  deleteComment() {
    this.commentService.deleteComment(this.comment._id);
  }
}
