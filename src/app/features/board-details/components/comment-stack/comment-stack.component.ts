import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import Task from '@shared/interfaces/task'
import Comment from '@shared/interfaces/comment'
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-comment-stack',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './comment-stack.component.html',
  styleUrls: ['./comment-stack.component.scss']
})
export class CommentStackComponent implements OnInit {
  fetching$: Observable<boolean> = this.commentService.fetching$;
  comments$: Observable<Comment[]> = this.commentService.comments$;
  newCommentFormHidden: boolean = true;
  newCommentForm!: FormGroup;

  constructor(
    private commentService: CommentService,
    private fb: FormBuilder,
  ) { }

  ngOnInit(): void {
    this.newCommentForm = this.fb.group({
      text: new FormControl('', [
        Validators.required,
        Validators.minLength(1),
      ])
    });
  }

  get selectedTask(): Task | null {
    return this.commentService.selectedTask;
  }

  isCommentsEmpty(): boolean {
    return this.commentService.comments.length === 0;
  }

  toggleNewComment(): void {
    this.newCommentFormHidden = !this.newCommentFormHidden;
  }

  newComment(): void {
    this.commentService.newComment(this.newCommentForm.value.text);
    this.newCommentForm.reset({ text: '' });
  }

}
