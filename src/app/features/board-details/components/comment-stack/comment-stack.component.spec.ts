import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CommentStackComponent } from './comment-stack.component';

describe('CommentStackComponent', () => {
  let component: CommentStackComponent;
  let fixture: ComponentFixture<CommentStackComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CommentStackComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(CommentStackComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
