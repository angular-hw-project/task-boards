import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import Task from '@shared/interfaces/task';
import { CdkDragDrop, transferArrayItem } from '@angular/cdk/drag-drop';
import List from '@shared/interfaces/list';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BoardService } from '../../services/board.service';
import Board from '@shared/interfaces/board';
import { Observable, Subscription } from 'rxjs';
import Sort from '@shared/interfaces/sort';

@Component({
  selector: 'app-task-list',
  templateUrl: './task-list.component.html',
  styleUrls: ['./task-list.component.scss']
})
export class TaskListComponent implements OnInit, OnDestroy {
  @Input() list!: List;
  @Input() dragEnabled: boolean = false;

  columnColor: string = '';
  currentColor: string = '';
  fetching$: Observable<boolean> = this.boardService.fetching$;
  newTaskFormHidden: boolean = true;
  newTaskForm!: FormGroup;
  board$!: Subscription;
  board: Board | null = this.boardService.board;
  sort$!: Subscription;
  sort: Sort = this.boardService.sort;

  constructor(
    private fb: FormBuilder,
    private boardService: BoardService
  ) { }

  ngOnInit(): void {
    this.columnColor = this.list.color;
    this.currentColor = this.list.color;
    this.newTaskForm = this.fb.group({
      name: new FormControl('', [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ])
    });

    this.board$ = this.boardService.board$.subscribe((value) => {
      this.board = value;
      if (this.board !== null && this.list.status !== '') {
        this.list.color = this.board.color_scheme[this.list.status];
      }
      this.columnColor = this.list.color;
    });
    this.sort$ = this.boardService.sort$.subscribe((value) => {
      this.sort = value;
    })
  }

  ngOnDestroy() {
    this.board$.unsubscribe();
    this.sort$.unsubscribe();
  }

  changeColor() {
    if (this.columnColor === this.list.color || this.list.status === '') return;
    this.boardService.patchBoardColor(this.list.status, this.columnColor)
  }

  newTask() {
    this.boardService.newTask(this.newTaskForm.value.name, this.list.status);
    this.newTaskForm.reset({ name: '' });
  }

  drop(event: CdkDragDrop<Task[]>) {
    if (event.previousContainer === event.container) return;
    transferArrayItem(event.previousContainer.data,
      event.container.data,
      event.previousIndex,
      event.currentIndex);
    this.boardService.changeTaskStatus(event.container.data[event.currentIndex]._id, this.list.status);
  }

  toggleNewTask(): void {
    this.newTaskFormHidden = !this.newTaskFormHidden;
  }

}
