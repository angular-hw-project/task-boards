import { ChangeDetectionStrategy, Component, Input, OnInit } from '@angular/core';
import Task from '@shared/interfaces/task';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { BoardService } from '../../services/board.service';
import { Observable } from 'rxjs';
import { CommentService } from '../../services/comment.service';

@Component({
  selector: 'app-task-item',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './task-item.component.html',
  styleUrls: ['./task-item.component.scss']
})
export class TaskItemComponent implements OnInit {
  @Input() task!: Task;
  @Input() color!: string;

  menuHidden: boolean = true;
  editFormHidden: boolean = true;
  editForm!: FormGroup;
  fetching$: Observable<boolean> = this.boardService.fetching$;

  constructor(
    private boardService: BoardService,
    private commentService: CommentService,
    private fb: FormBuilder
  ) { }

  ngOnInit(): void {
    this.editForm = this.fb.group({
      name: new FormControl(this.task.name, [
        Validators.required,
        Validators.minLength(3),
        Validators.maxLength(100)
      ])
    });
  }

  setCommentTask(): void {
    this.commentService.setSelectedTask(this.task);
  }

  toggleMenu(): void {
    this.menuHidden = !this.menuHidden;
  }

  toggleArchive(): void {
    this.boardService.toggleArchivedStatus(this.task._id);
  }

  deleteTask(): void {
    this.boardService.deleteTask(this.task._id);
  }

  toggleEdit(): void {
    this.editFormHidden = !this.editFormHidden;
  }

  editTask(): void {
    this.boardService.editTask(this.task._id, this.editForm.value.name);
  }
}
