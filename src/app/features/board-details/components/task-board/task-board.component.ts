import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subscription } from 'rxjs';
import Option from '@shared/interfaces/option';
import Task from '@shared/interfaces/task';
import List from '@shared/interfaces/list';
import Filter from '@shared/interfaces/filter';
import Board from '@shared/interfaces/board';
import Sort from '@shared/interfaces/sort';
import { BoardService } from '../../services/board.service';

@Component({
  selector: 'app-task-board',
  templateUrl: './task-board.component.html',
  styleUrls: ['./task-board.component.scss']
})
export class TaskBoardComponent implements OnInit, OnDestroy {
  fetching$: Observable<boolean> = this.boardService.fetching$;
  board$!: Subscription;
  board: Board | null = null;
  boardNotFound$: Observable<boolean> = this.boardService.boardNotFound$;
  lists$: Observable<List[]> = this.boardService.lists$;
  archivedList$!: Subscription;
  archivedList: List = this.boardService.archivedList;

  private route$!: Subscription;

  filterOptions: Option[] = [
    { alias: 'name', description: 'Task name' }
  ];
  sortOptions: Option[] = [
    { alias: 'createdDate', description: 'Creation date' },
    { alias: 'name', description: 'Task name' }
  ];

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private boardService: BoardService
  ) { }

  ngOnInit(): void {
    this.route$ = this.route.params.subscribe((params) => {
      this.boardService.setBoardId(params['id']);
      this.refreshBoard();
    });
    this.archivedList$ = this.boardService.archivedList$.subscribe(list => {
      this.archivedList = list;
    });
    this.board$ = this.boardService.board$.subscribe(board => {
      this.board = board;
    });
  }

  ngOnDestroy(): void {
    this.route$.unsubscribe();
    this.archivedList$.unsubscribe();
    this.board$.unsubscribe();
  }

  refreshBoard() {
    this.boardService.fetchBoard();
  }

  refreshTasks() {
    this.boardService.fetchTasks();
  }

  redirect(path: string): void {
    this.router.navigate([path]);
  }

  get defaultFilter(): Filter {
    return this.boardService.defaultFilter;
  }

  get defaultSort(): Sort {
    return this.boardService.defaultSort;
  }

  setFilter(filter: Filter): void {
    this.boardService.setFilter(filter);
  }

  setSort(sort: Sort): void {
    this.boardService.setSort(sort);
  }

}
