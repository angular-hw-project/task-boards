import { Injectable } from '@angular/core';
import Board from '@shared/interfaces/board';
import Task from '@shared/interfaces/task';
import Comment from '@shared/interfaces/comment';
import Filter from '@shared/interfaces/filter';
import { environment } from '@env';
import { BehaviorSubject, catchError, Observable, retry, tap } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { HandleError, HttpErrorHandlerService } from './http-error-handler.service';
import MsgResponse from '@shared/interfaces/msgResponse';
import User from '@shared/interfaces/user';
import RegisterBody from '@shared/interfaces/registerBody';

interface Login {
  message: string;
  jwt_token: string;
}

@Injectable({
  providedIn: 'root'
})
export class RestApiService {
  private handleError: HandleError;
  private _fetching$: BehaviorSubject<boolean>;
  fetching$: Observable<boolean>;

  get fetching(): boolean {
    return this._fetching$.getValue();
  }

  setFetching(nextState: boolean): void {
    this._fetching$.next(nextState);
  }

  constructor(
    private http: HttpClient,
    private httpErrorHandler: HttpErrorHandlerService
  ) {
    this.handleError = httpErrorHandler.createHandleError('RestApiService');
    this._fetching$ = new BehaviorSubject(false);
    this.fetching$ = this._fetching$.asObservable();
  }

  getBoards(filter: Filter): Observable<Board[]> {
    this.setFetching(true);
    const url = `${environment.apiUrl}boards?filterType=${filter.filter_by}&filterText=${filter.filter}`;
    return this.http.get<Board[]>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('getBoards', [])),
        tap(() => { this.setFetching(false) })
      );
  }

  getBoard(id: string): Observable<Board | null> {
    this.setFetching(true);
    return this.http.get<Board | null>(`${environment.apiUrl}boards/${id}`)
      .pipe(
        retry(3),
        catchError(this.handleError('getBoard', null)),
        tap(() => { this.setFetching(false) })
      );
  }

  newBoard(data: { name: string, description: string }): Observable<MsgResponse> {
    this.setFetching(true);
    return this.http.post<MsgResponse>(`${environment.apiUrl}boards/`, data)
      .pipe(
        retry(3),
        catchError(this.handleError('newBoard', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  editBoard(id: string, data: { name: string, description: string }): Observable<MsgResponse> {
    this.setFetching(true);
    return this.http.put<MsgResponse>(`${environment.apiUrl}boards/${id}`, data)
      .pipe(
        retry(3),
        catchError(this.handleError('editBoard', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  patchBoardColor(id: string, colorName: string, color: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}boards/${id}/colors/${colorName}`;
    return this.http.patch<MsgResponse>(url, { color: color })
      .pipe(
        retry(3),
        catchError(this.handleError('editBoard', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  deleteBoard(id: string): Observable<MsgResponse> {
    this.setFetching(true);
    return this.http.delete<MsgResponse>(`${environment.apiUrl}boards/${id}`)
      .pipe(
        retry(3),
        catchError(this.handleError('deleteBoard', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  getTasks(board_id: string, filter: Filter): Observable<Task[]> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/board/${board_id}?filterType=${filter.filter_by}&filterText=${filter.filter}`;
    return this.http.get<Task[]>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('getTasks', [])),
        tap(() => { this.setFetching(false) })
      );
  }

  newTask(board_id: string, name: string, status: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/`;
    return this.http.post<MsgResponse>(url, { board_id, name, status })
      .pipe(
        retry(3),
        catchError(this.handleError('addTask', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  editTask(id: string, name: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/${id}`;
    return this.http.put<MsgResponse>(url, { name })
      .pipe(
        retry(3),
        catchError(this.handleError('editTask', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  changeTaskStatus(id: string, status: Task['status']): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/${id}`;
    return this.http.put<MsgResponse>(url, { status })
      .pipe(
        retry(3),
        catchError(this.handleError('changeTaskStatus', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  toggleArchivedStatus(id: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/${id}/archived/toggle`;
    return this.http.patch<MsgResponse>(url, {})
      .pipe(
        retry(3),
        catchError(this.handleError('toggleArchivedStatus', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  deleteTask(id: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}tasks/${id}`;
    return this.http.delete<MsgResponse>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('deleteTask', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  getComments(task_id: string): Observable<Comment[]> {
    this.setFetching(true);
    const url = `${environment.apiUrl}comments/${task_id}`;
    return this.http.get<Comment[]>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('getComments', [])),
        tap(() => { this.setFetching(false) })
      );
  }

  newComment(task_id: string, text: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}comments/`;
    return this.http.post<MsgResponse>(url, { task_id, text })
      .pipe(
        retry(3),
        catchError(this.handleError('getComments', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  deleteComment(id: string): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}comments/${id}`;
    return this.http.delete<MsgResponse>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('deleteComment', { message: 'Something went wrong.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  getUserToken(email: string, password: string): Observable<Login> {
    this.setFetching(true);
    const url = `${environment.apiUrl}auth/login/`;
    return this.http.post<Login>(url, { email, password })
      .pipe(
        retry(3),
        catchError(this.handleError('getUserToken', { message: 'Unauthorized.', jwt_token: '' })),
        tap(() => { this.setFetching(false) })
      );
  }

  registerUser(credentials: RegisterBody): Observable<MsgResponse> {
    this.setFetching(true);
    const url = `${environment.apiUrl}auth/register/`;
    return this.http.post<MsgResponse>(url, credentials)
      .pipe(
        retry(3),
        catchError(this.handleError('registerUser', { message: 'Unauthorized.' })),
        tap(() => { this.setFetching(false) })
      );
  }

  getUser(): Observable<User | null> {
    this.setFetching(true);
    const url = `${environment.apiUrl}auth/`;
    return this.http.get<User | null>(url)
      .pipe(
        retry(3),
        catchError(this.handleError('getUser', null)),
        tap(() => { this.setFetching(false) })
      );
  }

}
