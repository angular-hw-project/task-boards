import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { HandleError, HttpErrorHandlerService } from './http-error-handler.service';
import { Router } from '@angular/router';
import User from '@shared/interfaces/user';
import { RestApiService } from './rest-api.service';
import RegisterBody from '@shared/interfaces/registerBody';
import FetchMessage from '@shared/interfaces/fetchMessage';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private handleError: HandleError;
  private initialFetchMessage: FetchMessage = { status: 'error', message: '' };

  private _fetching$: BehaviorSubject<boolean>;
  private _user$: BehaviorSubject<User | null>;
  private _fetchMessage$: BehaviorSubject<FetchMessage>;

  fetching$: Observable<boolean>;
  user$: Observable<User | null>;
  fetchMessage$: Observable<FetchMessage>;

  constructor(
    private httpErrorHandler: HttpErrorHandlerService,
    private router: Router,
    private restApi: RestApiService
  ) {
    this.handleError = httpErrorHandler.createHandleError('AuthService');
    this._fetching$ = new BehaviorSubject(false);
    this._user$ = new BehaviorSubject<User | null>(null);
    this._fetchMessage$ = new BehaviorSubject<FetchMessage>(this.initialFetchMessage)

    this.fetching$ = this._fetching$.asObservable();
    this.user$ = this._user$.asObservable();
    this.fetchMessage$ = this._fetchMessage$.asObservable();
  }

  get fetching(): boolean {
    return this.restApi.fetching;
  }

  setFetching(nextState: boolean): void {
    this.restApi.setFetching(nextState);
  }

  get user(): User | null {
    return this._user$.getValue();
  }

  setUser(nextState: User | null): void {
    this._user$.next(nextState);
  }

  get fetchMessage(): FetchMessage {
    return this._fetchMessage$.getValue();
  }

  setFetchMessage(nextState: FetchMessage): void {
    this._fetchMessage$.next(nextState);
  }

  set authToken(value: string) {
    localStorage.setItem('authorization', value);
  }

  get authToken(): string {
    const token = localStorage.getItem('authorization');
    if (!token) return '';
    return token;
  }

  isLoggedIn(): boolean {
    if (this.authToken !== '' && this.user === null) {
      this.login()
      return true;
    }
    return this.user !== null;
  }

  getAuthToken(email: string, password: string): void {
    this.setFetchMessage(this.initialFetchMessage);
    this.restApi.getUserToken(email, password).subscribe((res) => {
      this.authToken = res.jwt_token;
      if (this.authToken === '') {
        const message = res.message;
        this.setFetchMessage({ status: 'error', message });
        return;
      }
      if (this.authToken !== '') {
        this.setFetchMessage({ status: 'success', message: 'Success' });
      }
      this.login();
    });
  }

  private login(): void {
    this.restApi.getUser().subscribe(res => {
      if (res !== null) {
        this.setUser(res);
        this.router.navigate(['/dashboard']);
      }
    })
  }

  register(credentials: RegisterBody): void {
    this.setFetchMessage(this.initialFetchMessage);
    this.restApi.registerUser(credentials).subscribe((res) => {
      if (res.message === 'Success') {
        this.setFetchMessage({ status: 'success', message: res.message });
        this.router.navigate(['/auth/login']);
      }
      this.setFetchMessage({ status: 'error', message: res.message });
    });
  }

  logout() {
    this.authToken = '';
    this.setUser(null);
    this.router.navigate(['/auth/login']);
  }
}
