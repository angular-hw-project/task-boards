# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

## [1.0.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.10.3...v1.0.0) (2022-11-14)


### Features

* **app:** deploy to firebase ([0982c45](https://gitlab.com/angular-hw-project/task-boards/commit/0982c4535558c2637732218d5b0b8c1919f1f4f2))

### [0.10.3](https://gitlab.com/angular-hw-project/task-boards/compare/v0.10.2...v0.10.3) (2022-11-14)


### Bug Fixes

* **dashboard:** fix empty Boards[] array onInit ([9f32314](https://gitlab.com/angular-hw-project/task-boards/commit/9f323141ddd5920bf722f6dc5596aa69d356a518))

### [0.10.2](https://gitlab.com/angular-hw-project/task-boards/compare/v0.10.1...v0.10.2) (2022-11-14)


### Bug Fixes

* **auth:** reset fetchMessage on init ([4e4e203](https://gitlab.com/angular-hw-project/task-boards/commit/4e4e20398b4baae7c8633a13785fc836b3246e87))

### [0.10.1](https://gitlab.com/angular-hw-project/task-boards/compare/v0.10.0...v0.10.1) (2022-11-14)


### Features

* **auth:** add Login and Register component styles ([ce7d329](https://gitlab.com/angular-hw-project/task-boards/commit/ce7d329c03a8ba4922a88f8eb9678888baa53704))
* **dashboard:** add unauthorized plug ([8bebd2b](https://gitlab.com/angular-hw-project/task-boards/commit/8bebd2bfaa1091180d9d91c0eddc5fa83a318260))

## [0.10.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.9.0...v0.10.0) (2022-11-14)


### Features

* **board-details:** add BoardDetails styles ([f6c8e56](https://gitlab.com/angular-hw-project/task-boards/commit/f6c8e568efe3a8e67d7b32becedb039c5ebfa5ec))

## [0.9.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.8.2...v0.9.0) (2022-11-14)


### Features

* **dashboard:** add Dashboard styles ([97eee34](https://gitlab.com/angular-hw-project/task-boards/commit/97eee34a24258d5c832a3dae57bfbba77b4f7b8c))

### [0.8.2](https://gitlab.com/angular-hw-project/task-boards/compare/v0.8.1...v0.8.2) (2022-11-14)


### Bug Fixes

* **core/services:** fix isLoggedIn method ([76b07bb](https://gitlab.com/angular-hw-project/task-boards/commit/76b07bb81fd836215109cc6eadcecc4fc0409c4a))

### [0.8.1](https://gitlab.com/angular-hw-project/task-boards/compare/v0.8.0...v0.8.1) (2022-11-13)


### Bug Fixes

* **header:** fix username render ([94484e1](https://gitlab.com/angular-hw-project/task-boards/commit/94484e1167950e243b95e98a1d35f0bdc7a9afe7)), closes [#3](https://gitlab.com/angular-hw-project/task-boards/issues/3)

## [0.8.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.7.0...v0.8.0) (2022-11-13)


### Features

* **board-details:** add Board, Comment services ([dbb8397](https://gitlab.com/angular-hw-project/task-boards/commit/dbb83970664dd4a402bcb4b259c85ef03534da80))

## [0.7.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.6.0...v0.7.0) (2022-11-12)


### Features

* **dashboard:** add BoardModal, Dashboard services ([2e1e43a](https://gitlab.com/angular-hw-project/task-boards/commit/2e1e43a24f426929081826e026201d050f8bdd2b))

## [0.6.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.5.0...v0.6.0) (2022-11-11)


### Features

* **list-filters:** add ListFilters component ([47306c6](https://gitlab.com/angular-hw-project/task-boards/commit/47306c64a58fd9fc53787dc80b6859b8fbbb9389))
* **list-filters:** add ListFilters component ([7ad3c55](https://gitlab.com/angular-hw-project/task-boards/commit/7ad3c55d0f533504542b7684fd4ff94e4eb7999a))

## [0.5.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.4.0...v0.5.0) (2022-11-11)


### Features

* **header:** add Header component ([0f1c8cb](https://gitlab.com/angular-hw-project/task-boards/commit/0f1c8cb5b08af787f95084fdde989a94fa9f116d))

## [0.4.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.3.0...v0.4.0) (2022-11-11)


### Features

* **auth.guard:** add Auth guard ([232dae1](https://gitlab.com/angular-hw-project/task-boards/commit/232dae1c516ed9fd02d17a3906eed42b1e6add36))
* **guest.guard:** add Guest guard ([5cde729](https://gitlab.com/angular-hw-project/task-boards/commit/5cde729ce7e7643f40dd296abf15343038943e3a))

## [0.3.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.2.0...v0.3.0) (2022-11-11)


### Features

* **board-details:** add BoardDetails component ([41eefc4](https://gitlab.com/angular-hw-project/task-boards/commit/41eefc411a9c0772bb75e39d89b50e11428cbea7))

## [0.2.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.5...v0.2.0) (2022-11-11)


### Features

* **auth:** add Auth component and routes ([f59b94a](https://gitlab.com/angular-hw-project/task-boards/commit/f59b94a9c92411c2108565a0609ad07cce5b7aac))

### [0.1.5](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.4...v0.1.5) (2022-11-11)


### Bug Fixes

* **auth-header.interceptor:** fix request update data ([8241e7d](https://gitlab.com/angular-hw-project/task-boards/commit/8241e7d3c084981f0f0856b4a201d13ad15f2988))

### [0.1.4](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.3...v0.1.4) (2022-11-11)


### Bug Fixes

* **core.module.ts:** add missing modules ([6ee3a5d](https://gitlab.com/angular-hw-project/task-boards/commit/6ee3a5d2b8cacf39d63b454e77437a2e3810ee4e)), closes [#2](https://gitlab.com/angular-hw-project/task-boards/issues/2)
* **environment:** change apiUrl value ([f764a5b](https://gitlab.com/angular-hw-project/task-boards/commit/f764a5be6a1a0ae78c92785f0d1c92004de8dfe5))

### [0.1.3](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.2...v0.1.3) (2022-11-11)


### Features

* **dashboard:** add Dashboard component ([e74b510](https://gitlab.com/angular-hw-project/task-boards/commit/e74b510c708ee8ea642e5e404fa2ae4240ddfe75))

### [0.1.2](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.1...v0.1.2) (2022-11-11)


### Features

* **page-not-found:** add PageNotFound component ([23dd799](https://gitlab.com/angular-hw-project/task-boards/commit/23dd7992fbb5917647c58ec687f9aec40e1c0e08))

### [0.1.1](https://gitlab.com/angular-hw-project/task-boards/compare/v0.1.0...v0.1.1) (2022-11-10)


### Bug Fixes

* **core:** add http interceptor for authorization ([2022d1f](https://gitlab.com/angular-hw-project/task-boards/commit/2022d1f659b189143ba58e8552ff1633d9483238)), closes [#1](https://gitlab.com/angular-hw-project/task-boards/issues/1)

## [0.1.0](https://gitlab.com/angular-hw-project/task-boards/compare/v0.0.3...v0.1.0) (2022-11-10)


### Features

* **core:** add basic services ([ed65099](https://gitlab.com/angular-hw-project/task-boards/commit/ed65099611b1f87993b6400c29307c532d993163))
* **shared:** add interfaces ([4dda637](https://gitlab.com/angular-hw-project/task-boards/commit/4dda637f09ac49fca57b2925c8ede722ca888927))

### [0.0.3](https://gitlab.com/angular-hw-project/task-boards/compare/v0.0.2...v0.0.3) (2022-11-10)

### [0.0.2](https://gitlab.com/angular-hw-project/task-boards/compare/v0.0.1...v0.0.2) (2022-11-10)

### [0.0.1](https://gitlab.com/angular-hw-project/task-boards/compare/v0.0.0...v0.0.1) (2022-11-10)


### Bug Fixes

* **package.json:** add standard-version devDependency ([a74e19c](https://gitlab.com/angular-hw-project/task-boards/commit/a74e19c5a19411da3d7c831075cac2aabe4211cf))

## 0.0.0 (2022-11-10)
